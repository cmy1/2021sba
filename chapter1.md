### 4: 打開終端機
2. 在mac中，把我們的檔案夾拉到dock中的終端機圖示上放開
![](https://i.imgur.com/qmZOxGL.png)
3. 應該會看到如下的圖片
![](https://i.imgur.com/vZLILbp.png)
> 更多終端機教學請看[這裡](https://gitbook.tw/chapters/command-line/command-line.html)

4. 在終端機裡輸入`gitbook install`安裝plugin
![](https://i.imgur.com/07Fx0Z6.png)
5. 在終端機裡輸入`gitbook serve`發佈這本書
![](https://i.imgur.com/AHLT1oc.png)

### 5: 完成，在Chrome中貼上[http://localhost:4000](http://localhost:4000)就可以開啟書了
![](https://i.imgur.com/nuB1MKJ.png)

## 第三部分：如何編輯Gitbook
### 1: 新增文件
1. 點開README.md檔案來開啟typora，在左側側邊欄會看到這個資料夾下的所有檔案
![](https://i.imgur.com/2L9Djcw.png)
2. 按左下角有一個`+`號，新增一個叫chapter1的文件
![](https://i.imgur.com/iUA1Roc.png)
3. 在這裡我們就可以開始用[Markdown語法](https://markdown.tw/)來編輯文件了

![](https://i.imgur.com/AgUtHiF.png)
### 2: 編輯`SUMMARY.md`
1. 我們還要把這個文件加入我們的目錄中，請打開`SUMMARY.md`, 並依照項目符號加入檔案，好了之後存檔
``` = markdown
-[顯示名稱](檔名.md)
-[顯示名稱](子資料夾/檔名.md)
